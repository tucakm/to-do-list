package com.examples.tucakm.to_do_list_projekt;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by tucakm on 24.06.2018..
 */

public class PregledAdapter extends CursorAdapter {
    TextView naslovTV;
    TextView vrijemeTV;
    TextView datumTV;
    TextView timeLeftTV;
    TextView tagTV;
    ImageView slika;


    private Cursor cur;
    public PregledAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
        cur=cursor;

    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.pregled_redak_liste, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor){
        naslovTV = (TextView) view.findViewById(R.id.naslovID);
        vrijemeTV = (TextView) view.findViewById(R.id.vrijemeID);
        datumTV = (TextView) view.findViewById(R.id.datumID);
        timeLeftTV=(TextView) view.findViewById(R.id.timeLeftID);
        tagTV=(TextView) view.findViewById(R.id.tagID);
        slika=(ImageView) view.findViewById(R.id.imageView3);


        String naslov=cursor.getString(cursor.getColumnIndex("naslov_obaveze"));
        String vrijeme=cursor.getString(cursor.getColumnIndex("vrijeme_roka"));
        String datum=cursor.getString(cursor.getColumnIndex("datum_roka"));
        String tag=cursor.getString(cursor.getColumnIndex("tag"));
        String priority=cursor.getString(cursor.getColumnIndex("priority"));
        String vrijemeUnosa=cursor.getString(cursor.getColumnIndex("vrijeme_unosa"));
        int active=cursor.getInt(cursor.getColumnIndex("active"));

        if(priority.equals("Niski")){
            slika.setBackgroundColor(Color.GREEN);

        }
        else if(priority.equals("Srednji")){
            slika.setBackgroundColor(Color.YELLOW);
        }
        else{
            slika.setBackgroundColor(Color.RED);
        }
        naslovTV.setText(naslov);
        vrijemeTV.setText(vrijeme);
        datumTV.setText(datum);
        tagTV.setText(tag);
        timeLeftTV.setText(vrijemeUnosa+"d");
    }


    public Obaveze getItem(int position) {
        cur.moveToPosition(position);
        return new Obaveze(cur.getInt(cur.getColumnIndex("_id")),
                cur.getString(cur.getColumnIndex("naslov_obaveze")),
                cur.getString(cur.getColumnIndex("priority")),
                cur.getString(cur.getColumnIndex("tag")),
                cur.getString(cur.getColumnIndex("vrijeme_unosa")),
                cur.getString(cur.getColumnIndex("vrijeme_roka")),
                cur.getString(cur.getColumnIndex("datum_roka")),
                cur.getInt(cur.getColumnIndex("active"))
        );
    }
}
