package com.examples.tucakm.to_do_list_projekt;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
public class ObavezeInfo extends AppCompatActivity {
    private TextView naziv,preostaloVrijeme,vr,dr,prio;
    private EditText t,nazivObaveza;
    private CheckBox dovrs;
    private int id;
    private Obaveze obaveze;
    private ToDoListHelper toDoListHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obaveze_info);
        init();
        obaveze= (Obaveze) new Gson().fromJson(getIntent().getStringExtra("obaveza"),Obaveze.class);
        puni(obaveze);
        dovrs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dovrs.isChecked()){
                    toDoListHelper.changeActive(obaveze.get_id(),0);
                    Toast.makeText(ObavezeInfo.this, "Uspješno ste izvršili obavezu pod nazivom:'"+obaveze.getNaslov_obaveze()+"'", Toast.LENGTH_LONG).show();
                    Intent i= new Intent(ObavezeInfo.this,MainActivity.class);
                    startActivity(i);

                }else{
                    toDoListHelper.changeActive(obaveze.get_id(),1);
                    Toast.makeText(ObavezeInfo.this, "Uspješno ste resetirali obavezu pod nazivom:'"+obaveze.getNaslov_obaveze()+"'", Toast.LENGTH_LONG).show();
                    Intent i= new Intent(ObavezeInfo.this,MainActivity.class);
                    startActivity(i);

                }
            }
        });
    }

    private void puni(Obaveze o){
        if(o!=null) {
            id=o.get_id();
            nazivObaveza.setText(o.getNaslov_obaveze());
            String dani=o.getVrijemePreostalo()+"dana";
            preostaloVrijeme.setText(dani);
            t.setText(o.getTag());
            prio.setText(o.getPriority());

            if (o.getPriority().equals("Niski")) {
                prio.setTextColor(Color.GREEN);

            } else if (o.getPriority().equals("Srednji")) {
                prio.setTextColor(Color.YELLOW);
            } else {
                prio.setTextColor(Color.RED);
            }
            vr.setText(o.getVrijemeRoka());
            dr.setText(o.getDatumRoka());

            if (o.getActive() != 0) {
                dovrs.setChecked(false);
            } else {
                dovrs.setChecked(true);
            }
        }
    }
    private void init(){
        nazivObaveza=(EditText)findViewById(R.id.editNaziv);
        preostaloVrijeme=(TextView)findViewById(R.id.editPreostalo);
        t=(EditText)findViewById(R.id.editTag);
        prio= (TextView) findViewById(R.id.editPrioritet1);
        vr=(TextView) findViewById(R.id.editVrijeme);
        dr=(TextView) findViewById(R.id.editDatum);
        dovrs=(CheckBox)findViewById(R.id.checkBox);
        toDoListHelper=new ToDoListHelper(this);

    }
    public void pickTime(View v){
        TimePickerFragment timeFragment= new TimePickerFragment();
        timeFragment.setTextView(v);
        timeFragment.show(getSupportFragmentManager(),"Vrijeme");
    }
    public void pickDate(View v){
        DatePickerFragment datePickerFragment= new DatePickerFragment();
        datePickerFragment.setTextView(v);
        datePickerFragment.show(getSupportFragmentManager(),"Datum");
    }

    public void izmjeni(View v){
        preostaloVrijeme.setText(DateTimer.CalculateDateBetween(dr.getText().toString()));

        if(id==0 || nazivObaveza.getText().toString()=="" || t.getText().toString()=="" || vr.getText().toString()=="" || dr.getText().toString()==""){
            Snackbar.make(v, "Postoje prazna polja", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }
        else if(nazivObaveza.getText().toString().equals(obaveze.getNaslov_obaveze()) && t.getText().toString().equals(obaveze.getTag()) && vr.getText().toString().equals(obaveze.getVrijemeRoka()) && dr.getText().toString().equals(obaveze.getDatumRoka())){
            Snackbar.make(v, "Niste promjenili vrijednosti niti jednog polja", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }
        else {
            boolean update = toDoListHelper.changeData(new Obaveze(id,nazivObaveza.getText().toString(),prio.getText().toString(),t.getText().toString(),preostaloVrijeme.getText().toString(),vr.getText().toString(),dr.getText().toString(),1));
            if (update) {
                Snackbar.make(v, "Uspješno izmjenjeni podatci", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                return;
            } else {
                Snackbar.make(v, "Neuspješno izmjenjeni podatci", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                return;
            }
        }
    }
    public void izbrisi(View v){
            Obaveze o=new Obaveze(id,nazivObaveza.getText().toString(),prio.getText().toString(),t.getText().toString(),preostaloVrijeme.getText().toString(),vr.getText().toString(),dr.getText().toString(),1);
            boolean delete = toDoListHelper.deliteData(o);
            if (delete) {
                Toast.makeText(this, "Uspješno izbrisani podatci podatci '"+o.getNaslov_obaveze()+"'", Toast.LENGTH_LONG).show();
                Intent i= new Intent(this,MainActivity.class);
                startActivity(i);
                return;
            } else {
                Toast.makeText(this, "Nisu izbrisani podatci ", Toast.LENGTH_LONG).show();
                return;
            }

    }

    public void onBackPressed() {
        Intent i= new Intent(this, MainActivity.class);
        startActivity(i);
    }

}
