package com.examples.tucakm.to_do_list_projekt;

/**
 * Created by tucakm on 8.6.2018..
 */

public class Obaveze {
    private int _id;
    private String naslov_obaveze;
    private String priority;
    private String tag;
    private String vrijemePreostalo=""+0;
    private String vrijemeRoka;
    private String datumRoka;
    private int active;



    public Obaveze(String naslov_obaveze, String priority, String tag, String vrijemePreostalo, String vrijemeRoka, String datumRoka, int active){
        this.naslov_obaveze=naslov_obaveze;
        this.priority=priority;
        this.tag=tag;
        this.vrijemePreostalo = vrijemePreostalo;
        this.vrijemeRoka=vrijemeRoka;
        this.datumRoka=datumRoka;
        this.active=active;
    }
    public Obaveze(int _id,String naslov_obaveze,String priority,String tag,String vrijemeRoka,String datumRoka,int active){
        this._id=_id;
        this.naslov_obaveze=naslov_obaveze;
        this.priority=priority;
        this.tag=tag;
        this.vrijemePreostalo = vrijemePreostalo;
        this.vrijemeRoka=vrijemeRoka;
        this.datumRoka=datumRoka;
        this.active=active;
    }
    public Obaveze(int _id, String naslov_obaveze, String priority, String tag, String vrijemePreostalo, String vrijemeRoka, String datumRoka, int active){
        this._id=_id;
        this.naslov_obaveze=naslov_obaveze;
        this.priority=priority;
        this.tag=tag;
        this.vrijemePreostalo = vrijemePreostalo;
        this.vrijemeRoka=vrijemeRoka;
        this.datumRoka=datumRoka;
        this.active=active;
    }
    public Obaveze(int id,String vrijemePreostalo){
        this._id=id;
        this.vrijemePreostalo=vrijemePreostalo;
    }

    public String getDatumRoka() {
        return datumRoka;
    }

    public void setDatumRoka(String datumRoka) {
        this.datumRoka = datumRoka;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNaslov_obaveze() {
        return naslov_obaveze;
    }

    public void setNaslov_obaveze(String naslov_obaveze) {
        this.naslov_obaveze = naslov_obaveze;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getVrijemePreostalo() {
        return vrijemePreostalo;
    }

    public void setVrijemePreostalo(String vrijemePreostalo) {
        this.vrijemePreostalo = vrijemePreostalo;
    }

    public String getVrijemeRoka() {
        return vrijemeRoka;
    }

    public void setVrijemeRoka(String vrijemeRoka) {
        this.vrijemeRoka = vrijemeRoka;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
}
