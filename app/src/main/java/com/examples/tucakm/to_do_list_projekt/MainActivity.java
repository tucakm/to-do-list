package com.examples.tucakm.to_do_list_projekt;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private Intent intent;

    private ListView list;
    private TextView tv;
    private int active;
    private Intent i;
    private CursorAdapter cursorAdapter;
    private Cursor cursor;
    private ToDoListHelper toDoListHelper;
    private Obaveze o,ob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        active=1;
        list=(ListView)findViewById(R.id.list1);
        toDoListHelper=new ToDoListHelper(this);

        generateList(active);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> l, View view, int position, long id) {
                o=(Obaveze) list.getAdapter().getItem(position);
                Intent i = new Intent(MainActivity.this, ObavezeInfo.class);
                i.putExtra("obaveza",new Gson().toJson(o));
                startActivity(i);
            }
        });
        ob= (Obaveze) new Gson().fromJson(getIntent().getStringExtra("ovo"),Obaveze.class);
        if(ob!=null){
            Toast.makeText(MainActivity.this, "Uspješno izbrisana obaveza s nazivom: '"+ob.getNaslov_obaveze()+"'", Toast.LENGTH_LONG).show();
            return;
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent= new Intent(MainActivity.this,DodavanjeObavezaActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    //Sortovi
   private void generateList(int a){
       cursor=toDoListHelper.readData(""+a);
       reloadTime(cursor);
       cursorAdapter=new PregledAdapter(this,cursor);
       list.setAdapter(cursorAdapter);
       cursorAdapter.notifyDataSetChanged();
   }
   private void generateNameSortList(int a){
       cursor=toDoListHelper.readDataAscFromName(""+a);
       cursorAdapter=new PregledAdapter(this,cursor);
       list.setAdapter(cursorAdapter);
       cursorAdapter.notifyDataSetChanged();

   }
    private void generatePriorityList(int a){
        cursor=toDoListHelper.readDataFromPriority(""+a);
        cursorAdapter=new PregledAdapter(this,cursor);
        list.setAdapter(cursorAdapter);
        cursorAdapter.notifyDataSetChanged();

    }

    private void generateDovrseneObaveze(){
        cursor=toDoListHelper.readData(""+0);
        cursorAdapter=new PregledAdapter(this,cursor);
        list.setAdapter(cursorAdapter);
        cursorAdapter.notifyDataSetChanged();

    }
    @Override
    public void onStart(){
        reloadTime(cursor);
        super.onStart();
    }
    @Override
    public void onStop(){
        reloadTime(cursor);
        super.onStop();
    }
    @Override
    public void onDestroy(){
        reloadTime(cursor);
        super.onDestroy();
    }

   @Override
    protected void onActivityResult(int reqCode, int resCode, Intent data) {
        super.onActivityResult(reqCode, resCode, data);
        generateList(active);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nameSort) {
            generateNameSortList(active);

        } else if (id == R.id.nav_gallery) {
            generatePriorityList(active);

        } else if (id == R.id.nav_slideshow) {
            generateDovrseneObaveze();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void reloadTime(Cursor cursor1){

        if(cursor1!=null) {
            for (cursor1.moveToFirst(); !cursor1.isAfterLast(); cursor1.moveToNext()) {
                boolean update=toDoListHelper.changeTime(cursor1.getInt(cursor1.getColumnIndex("_id")), cursor1.getString(cursor1.getColumnIndex("datum_roka")));
                if(update){
                    Log.d("Dobro je ", "ili ne");
                }
                else {

                    Log.d("Nije je ", "ili ne");
                }
            }
        }
    }




}
