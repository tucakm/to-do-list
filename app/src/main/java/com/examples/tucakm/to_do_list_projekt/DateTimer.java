package com.examples.tucakm.to_do_list_projekt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTimer {

    public static long daysBetween(Date startDate, Date endDate) {
        Calendar sDate = Calendar.getInstance();
        Calendar eDate = Calendar.getInstance();
        sDate.setTime(startDate);
        eDate.setTime(endDate);

        long daysBetween = 0;
        while (sDate.before(eDate)) {
            sDate.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    public static String getToday(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        TimeZone timezone = TimeZone.getTimeZone("GMT+2");
        dateFormat.setTimeZone(timezone);
        Date date = new Date();

        return  dateFormat.format(date);
    }

    public static String CalculateDateBetween(String dateRoka){
        String dateNow=DateTimer.getToday();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date dan1=new Date();
        Date dan2=new Date();
        try {
            dan1= dateFormat.parse(dateNow);
            dan2= dateFormat.parse(dateRoka);
        }
        catch (ParseException e) {


        }
        long dani= DateTimer.daysBetween(dan1,dan2);

        String danii=""+dani;
        return danii;
    }


}