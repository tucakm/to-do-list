package com.examples.tucakm.to_do_list_projekt;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by tucakm on 20.6.2018..
 */

public class ToDoListHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "obaveze.db";
    private static final int DATABASE_VERSION = 1;

    public ToDoListHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_OBAVEZE = "create table obaveze ("
                + "_id integer primary key autoincrement, "
                + "naslov_obaveze text not null, "
                + "priority text not null, "
                + "tag text not null, "
                + "vrijeme_unosa text not null,"
                + "vrijeme_roka text not null,"
                + "datum_roka text not null,"
                + "active integer not null)";
        db.execSQL(CREATE_TABLE_OBAVEZE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table obaveze");
        onCreate(db);

    }
    public Cursor readData(String active) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT _id,naslov_obaveze,priority,tag,vrijeme_unosa,vrijeme_roka,datum_roka,active FROM obaveze WHERE  active=? ORDER BY _id DESC", new String[]{active});
        c.moveToFirst();
        db.close();
        return c;
    }
    public Cursor readDataAscFromName(String active) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT _id,naslov_obaveze,priority,tag,vrijeme_unosa,vrijeme_roka,datum_roka,active FROM obaveze WHERE  active=? ORDER BY naslov_obaveze ASC", new String[]{active});
        c.moveToFirst();
        db.close();
        return c;
    }
    public Cursor readDataFromPriority(String active) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT _id,naslov_obaveze,priority,tag,vrijeme_unosa,vrijeme_roka,datum_roka,active FROM obaveze WHERE  active=? ORDER BY priority DESC ", new String[]{active});
        c.moveToFirst();
        db.close();
        return c;
    }


    public boolean addData(Obaveze o){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put("naslov_obaveze",o.getNaslov_obaveze());
        contentValues.put("priority",o.getPriority());
        contentValues.put("tag",o.getTag());
        contentValues.put("vrijeme_unosa",o.getVrijemePreostalo());
        contentValues.put("vrijeme_roka",o.getVrijemeRoka());
        contentValues.put("datum_roka",o.getDatumRoka());
        contentValues.put("active",o.getActive());

        long result=db.insert("obaveze",null,contentValues);
        if(result==-1){
            return false;
        }
        else{
            return true;
        }

    }
    public boolean changeData(Obaveze o){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put("naslov_obaveze",o.getNaslov_obaveze());
        contentValues.put("priority",o.getPriority());
        contentValues.put("tag",o.getTag());
        contentValues.put("vrijeme_unosa",o.getVrijemePreostalo());
        contentValues.put("vrijeme_roka",o.getVrijemeRoka());
        contentValues.put("datum_roka",o.getDatumRoka());
        contentValues.put("active",o.getActive());
        String []id={""+o.get_id()};

        long result=db.update("obaveze",contentValues,"_id=?",id);
        if(result==-1){
            return false;
        }
        else{
            return true;
        }
    }

    public boolean changeActive(int idd,int active){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put("active",active);
        String []id={""+idd};

        long result=db.update("obaveze",contentValues,"_id=?",id);
        if(result==-1){
            return false;
        }
        else{
            return true;
        }
    }
    public boolean changeTime(int idd,String dateR){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put("vrijeme_unosa",DateTimer.CalculateDateBetween(dateR));
        String []id={""+idd};

        long result=db.update("obaveze",contentValues,"_id=?",id);
        if(result==-1){
            return false;
        }
        else{
            return true;
        }
    }
    public boolean deliteData(Obaveze o){
        SQLiteDatabase db= this.getWritableDatabase();
        String []id={""+o.get_id()};
        long result=db.delete("obaveze","_id=?",id);
        if(result==-1){
            return false;
        }
        else{
            return true;
        }
    }

}
