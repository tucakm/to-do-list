package com.examples.tucakm.to_do_list_projekt;


import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DodavanjeObavezaActivity extends AppCompatActivity {
    private TextView vrijeme,datum;
    private ImageView slika;
    private EditText naslov;
    private EditText tag;
    private NumberPicker priorityPicker;
    private String [] prioritet ={"Niski", "Srednji", "Visoki"};
    private String tipPrioriteta;
    private ToDoListHelper toDoListHelper;
    private Obaveze obaveze;
    private int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodavanje_obaveza);
        vrijeme=(TextView)findViewById(R.id.textView10);
        datum=(TextView)findViewById(R.id.textView9);
        naslov=(EditText)findViewById(R.id.editText2);
        tag=(EditText)findViewById(R.id.editText);
        slika=(ImageView)findViewById(R.id.imageView4);
        priorityPicker=(NumberPicker) findViewById(R.id.numberPicker2);
        priorityPicker.setMinValue(0);
        priorityPicker.setMaxValue(prioritet.length-1);
        priorityPicker.setDisplayedValues(prioritet);
        toDoListHelper=new ToDoListHelper(this);
        naslov.requestFocus();
        priorityPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                tipPrioriteta=prioritet[newVal];
                if(tipPrioriteta.equals("Niski")){
                    slika.setBackgroundColor(Color.GREEN);

                }else if(tipPrioriteta.equals("Srednji")){
                    slika.setBackgroundColor(Color.YELLOW);
                }
                else {
                    slika.setBackgroundColor(Color.RED);
                }

            }
        });
    }
    public void pickTime(View v){
        TimePickerFragment timeFragment= new TimePickerFragment();
        timeFragment.setTextView(v);
        timeFragment.show(getSupportFragmentManager(),"Vrijeme");
    }
    public void pickDate(View v){
        DatePickerFragment datePickerFragment= new DatePickerFragment();
        datePickerFragment.setTextView(v);
        datePickerFragment.show(getSupportFragmentManager(),"Datum");
    }
    public void dodaj(View v){
        String datetime=DateTimer.CalculateDateBetween(datum.getText().toString());

        if(naslov.getText().toString()=="" || tag.getText().toString()=="" || vrijeme.getText().toString().equals("Odaberi Vrijeme") || datum.getText().toString().equals("Odaberi Datum") || tipPrioriteta==""){
            Snackbar.make(v, "Unesite sve podatke", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }
        else{
            boolean insert=toDoListHelper.addData(new Obaveze(naslov.getText().toString(),tipPrioriteta,tag.getText().toString(),datetime,vrijeme.getText().toString(),datum.getText().toString(),1));
            if(insert){
                Snackbar.make(v, "Uspješno uneseni podatci", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent intent = new Intent(DodavanjeObavezaActivity.this,MainActivity.class);
                startActivity(intent);

                return;
            }
            else {
                Snackbar.make(v, "Dogodila se greška, nisi uneseni podatci", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                return;
            }
        }
    }
    public void onBackPressed(){
        super.onBackPressed();
    }
}
